import Cookies from 'js-cookie'

const TokenKey = 'Access-Token'
const NameKey = 'username'
const PassKey = 'password'
const AppKey = 'activeapp'

/**
 * 从cookies获取token
 */
export function getToken() {
  return Cookies.get(TokenKey)
}
/**
 * 保存Token到cookies
 */
export function setToken(token) {
  return Cookies.set(TokenKey, token)
}
/**
 * 从cookies删除token
 */
export function removeToken() {
  return Cookies.remove(TokenKey)
}
/**
 * 从cookies获取username
 */
export function getName() {
  return Cookies.get(NameKey)
}
/**
 * 保存username但cookies
 * @param {*} name 
 */
export function setName(name) {
  return Cookies.set(NameKey,name)
}
/**
 * 从cookies移除username
 */
export function removeName() {
  return Cookies.remove(NameKey)
}
/**
 * 从cookies获取password
 */
export function getPassWord() {
  return Cookies.get(PassKey)
}
/**
 * 保存password到cookies
 * @param {*} password 
 */
export function setPassWord(password) {
  return Cookies.set(PassKey,password)
}
/**
 * 从cookies移除password
 */
export function removePassWord() {
  return Cookies.remove(PassKey)
}

export function getActiveApp() {
  return Cookies.get(AppKey)
}

export function setActiveApp(activeApp) {
  return Cookies.set(AppKey, activeApp)
}

export function removeActiveApp() {
  return Cookies.remove(AppKey)
}
