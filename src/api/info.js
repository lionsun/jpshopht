import request from '@/utils/request'

/**
 * 获取应用详情
 */
export function merchantInfo() {
  return request({
    url: '/merchantInfo',
    method: 'get'
  })
}

/**
 * 修改密码
 */
export function updatePW(params) {
  return request({
    url: '/merchantInfo',
    method: 'put',
    data: params
  })
}