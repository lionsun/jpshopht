import request from '@/utils/request'


//设置

/**
 * 获取合伙人设置信息
 * @param {*} params 
 */
export function getPartnerSetting(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantAppInfo/'+id,
    method: 'get',
    params
  })
}
/**
 * 修改合伙人设置信息
 * @param {*} params 
 */
export function postPartnerSetting(params) {
  return request({
    url: '/openPartners',
    method: 'post',
    data: params
  })
}


//合伙人列表

/**
 * 获取合伙人列表
 * @param {*} params 
 */
export function getPartnerList(params) {
  return request({
    url: '/partners',
    method: 'get',
    params
  })
}
/**
 * 增加合伙人
 * @param {*} params 
 */
export function postPartner(params) {
  return request({
    url: '/partners',
    method: 'post',
    data: params
  })
}
/**
 * 修改合伙人
 * @param {*} params 
 */
export function putPartner(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/partners/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除合伙人
 * @param {*} params 
 */
export function delPartner(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/partners/'+id,
    method: 'delete',
    data: params
  })
}


//提现列表

/**
 * 提现
 * @param {*} params 
 */
export function getMoneny(params) {
  return request({
    url: '/partnerMerchantWithdraws',
    method: 'get',
    params
  })
}