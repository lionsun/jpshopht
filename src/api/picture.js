import request from '@/utils/request'

/**
 * 获取相册分组
 * @param {*} params 
 */
export function getPictureGroup(params) {
  return request({
    url: '/pictureGroup',
    method: 'get',
    params
  })
}
/**
 * 添加相册分组
 * @param {*} params 
 */
export function addPictureGroup(params) {
  return request({
    url: '/pictureGroup',
    method: 'post',
    data: params
  })
}
/**
 * 修改相册分组
 * @param {*} params 
 */
export function upPictureGroup(params) {
  return request({
    url: '/pictureGroup/' + params.id,
    method: 'put',
    data: params
  })
}
/**
 * 删除相册分组
 * @param {*} params 
 */
export function delPictureGroup(params) {
  return request({
    url: '/pictureGroup/' + params.id,
    method: 'delete',
    data: params
  })
}
/**
 * 获取相册图片
 * @param {*} params 
 */
export function getPictureByGroup(params) {
  return request({
    url: '/picture/'+params.id,
    method: 'get',
    params
  })
}
/**
 * 删除图片
 * @param {*} params 
 */
export function delPicture(params) {
  return request({
    url: '/merchantGoodsPicture/'+params.id,
    method: 'delete',
    data: params
  })
}
/**
 * 上传图片
 * @param {*} params 
 */
export function uploadPic(params) {
  return request({
    url: '/merchantGoodsPicture',
    method: 'post',
    data: params
  })
}