import request from '@/utils/request'
import qs from 'qs'

// export function login(data) {
//   return request({
//     url: '/user/login',
//     method: 'post',
//     data
//   })
// }

//登录
export function login(data) {
  return request({
    url: '/merchantLogin',
    method: 'post',
    data: data
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}


//注册

/**
 * 验证手机号是否被注册
 * @param {*} params 
 */
export function verifyPhone(params) {
  return request({
    url: '/merchantUsers',
    method: 'get',
    params
  })
}
/**
 * 获取手机验证码
 * @param {*} params 
 */
export function getPhoneCode(params) {
  return request({
    url: '/merchantsSmsCode',
    method: 'get',
    params
  })
}
/**
 * 注册
 * @param {*} params 
 */
export function register(params) {
  return request({
    url: '/merchantRegister',
    method: 'post',
    data: params
  })
}
