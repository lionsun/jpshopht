import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  //应用列表
  {
    path: '/',
    component: () => import('@/views/app'),
    hidden: true,
    children: [
      {
        path: '/',
        name: '应用列表',
        component: () => import('@/views/app/applist')
      },
      {
        path: '/appinfo',
        name: '应用信息',
        component: () => import('@/views/app/appinfo')
      }
    ]
  },

  //信息和修改密码
  {
    path: '/info/updatePW',
    component: () => import('@/views/info'),
    hidden: true,
    children: [
      {
        path: '/',
        name: '修改密码',
        component: () => import('@/views/info/updatePW')
      }
    ]
  },

  //总览
  {
    path: '/dashboard',
    component: Layout,
    redirect: '/dashboard',
    name: '总览',
    meta: { title: 'Dashboard', icon: 'a总览' },
    children: [
      {
        path: '/dashboard',
        name: '数据概览',
        component: () => import('@/views/dashboard'),
      },
      {
        path: '/upgrade',
        name: '升级日志',
        component: () => import('@/views/dashboard/uplog'),
      },
    ]
  },

  //商品
  {
    path: '/goods/list',
    component: Layout,
    redirect: '/goods/list',
    name: '商品',
    meta: { title: 'Example', icon: 'a商品' },
    children: [
      {
        path: '/goods/list',
        name: '商品列表',
        component: () => import('@/views/goods/goods/list'),
      },
      {
        path: '/goods/group',
        name: '商品分组',
        component: () => import('@/views/goods/goodsgroup'),
      },
      // {
      //   path: '/goods/area',
      //   name: '区域分组',
      //   component: () => import('@/views/goods/goodsarea'),
      // },
      {
        path: '/goods/recyclebin',
        name: '回收站',
        component: () => import('@/views/goods/recyclebin'),
      },
      {
        path: '/goods/add',
        name: '商品添加',
        component: () => import('@/views/goods/goods/Add'),
        hidden: true
      },
      // {
      //   path: '/goods/tinymce',
      //   name: '富文本',
      //   component: () => import('@/views/tinymce')
      // }
    ]
  },

  // 订单
  {
    path: '/order/manage',
    component: Layout,
    name: '订单',
    meta: { title: 'Form', icon: 'a订单' },
    children: [
      {
        path: '/order/manage',
        name: '订单管理',
        component: () => import('@/views/order/ordermanage')
      },
      {
        path: '/order/summary',
        name: '订单概述',
        component: () => import('@/views/order/ordersummary')
      },
      {
        path: '/order/comment',
        name: '评价管理',
        component: () => import('@/views/order/orderComment')
      },
      {
        path: '/order/print',
        name: '订单配送',
        component: () => import('@/views/order/orderPrint')
      }
    ]
  },

  // 会员
  {
    path: '/vip/list',
    component: Layout,
    name: '会员',
    meta: { title: 'Form', icon: 'a会员' },
    children: [
      {
        path: '/vip/list',
        name: '会员列表',
        component: () => import('@/views/vip/vipList')
      },
      {
        path: '/vip/unpay',
        name: '会员等级',
        component: () => import('@/views/vip/vipUnPay')
      }
    ]
  },

  //团长
  {
    path: '/customers/head',
    component: Layout,
    name: '团长',
    meta: { title: 'custormers', icon: 'a团长' },
    children: [
      {
        path: '/customers/head',
        name: '团长',
        component: () => import('@/views/customers/head')
      },
      {
        path: '/customers/audit',
        name: '团长审核',
        component: () => import('@/views/customers/audit')
      },
      {
        path: '/customers/leave',
        name: '团长等级',
        component: () => import('@/views/customers/leave')
      },
      // {
      //   path: '/customers/pusher',
      //   name: '推客',
      //   component: () => import('@/views/customers/pusher')
      // },
      // {
      //   path: '/customers/pusheraudit',
      //   name: '推客审核',
      //   component: () => import('@/views/customers/pusherAudit')
      // },
      // {
      //   path: '/customers/pusherleave',
      //   name: '推客等级',
      //   component: () => import('@/views/customers/pusherLeave')
      // },
      {
        path: '/customers/warehouse',
        name: '路线',
        component: () => import('@/views/customers/warehouse')
      },
      {
        path: '/setting/tuanconfig',
        name: '设置',
        component: () => import('@/views/setting/tuanconfig')
      },
      {
        path: '/finance/record',
        name: '佣金流水',
        component: () => import('@/views/finance/record')
      },
    ]
  },

  // 应用
  {
    path: '/marketing/index',
    component: Layout,
    name: '应用',
    meta: { title: 'custormers', icon: 'a应用' },
    children: [
      {
        path: '/marketing/index',
        name: '应用管理',
        component: () => import('@/views/marketing/index')
      },
      {
        path: '/marketing/coupon',
        name: '优惠券',
        component: () => import('@/views/marketing/coupon'),
        hidden: true
      },
      {
        path: '/marketing/buyCashback',
        name: '购物返现',
        component: () => import('@/views/marketing/buyCashback'),
        hidden: true
      },
      {
        path: '/marketing/seckill',
        name: '秒杀',
        component: () => import('@/views/marketing/seckill'),
        hidden: true
      },
      {
        path: '/marketing/singin',
        name: '签到',
        component: () => import('@/views/marketing/singin'),
        hidden: true
      },
      {
        path: '/marketing/apptamplate',
        name: '模板信息',
        component: () => import('@/views/marketing/apptemplate'),
        hidden: true
      },
      {
        path: '/marketing/assemble',
        name: '拼团',
        component: () => import('@/views/marketing/assemble'),
        hidden: true
      },
      {
        path: '/marketing/pay',
        name: '充值',
        component: () => import('@/views/marketing/pay'),
        hidden: true
      },
      {
        path: '/marketing/reduction',
        name: '满减',
        component: () => import('@/views/marketing/reduction'),
        hidden: true
      },
      {
        path: '/marketing/recuits',
        name: '新人专享',
        component: () => import('@/views/marketing/recuits'),
        hidden: true
      },
      {
        path: '/marketing/score',
        name: '积分商城',
        component: () => import('@/views/marketing/score'),
        hidden: true
      },
      {
        path: '/marketing/estimated',
        name: '预约送达',
        component: () => import('@/views/marketing/estimated'),
        hidden: true
      },
      {
        path: '/vip/pay',
        name: '会员PLUS',
        component: () => import('@/views/vip/vipPay'),
        hidden: true
      },
    ]
  },

  // 数据
  {
    path: '/finance/apply',
    component: Layout,
    name: '数据',
    meta: { title: 'custormers', icon: 'a数据' },
    children: [
      {
        path: '/finance/apply',
        name: '提现申请',
        component: () => import('@/views/finance/apply')
      },
      {
        path: '/statistics/sales',
        name: '销售统计',
        component: () => import('@/views/statistics/sales')
      },
      {
        path: '/statistics/goodsSales',
        name: '商品销售统计',
        component: () => import('@/views/statistics/goodsSales')
      },
      {
        path: '/statistics/leaderSales',
        name: '团长销售统计',
        component: () => import('@/views/statistics/leaderSales')
      },
      {
        path: '/statistics/userSales',
        name: '用户排行统计',
        component: () => import('@/views/statistics/userSales')
      },
    ]
  },

  // 小程序
  {
    path: '/applet/baseconfig',
    component: Layout,
    name: '小程序',
    meta: { title: 'custormers', icon: 'a小程序2' },
    children: [
      {
        path: '/applet/baseconfig',
        name: '支付',
        component: () => import('@/views/applet/baseconfig')
      },
      // {
      //   path: '/applet/baseinfo',
      //   name: '基础信息',
      //   component: () => import('@/views/applet/baseinfo')
      // },
    ]
  },

  // 权限
  {
    path: '/employee/manage',
    component: Layout,
    name: '权限',
    meta: { title: 'custormers', icon: 'a权限' },
    children: [
      {
        path: '/employee/manage',
        name: '员工管理',
        component: () => import('@/views/employee/manage')
      },
      {
        path: '/employee/role',
        name: '角色管理',
        component: () => import('@/views/employee/role')
      },
      {
        path: '/employee/kefu',
        name: '客服管理',
        component: () => import('@/views/employee/kefu')
      }
    ]
  },

  // 设置
  {
    path: '/setting/appsetting',
    component: Layout,
    name: '设置',
    meta: { title: 'custormers', icon: 'a设置' },
    children: [
      {
        path: '/setting/appsetting',
        name: '基础设置',
        component: () => import('@/views/setting/appsetting')
      },
      {
        path: '/setting/express',
        name: '运费模板',
        component: () => import('@/views/setting/express')
      },
      {
        path: '/setting/takegoods',
        name: '收货信息',
        component: () => import('@/views/setting/takegoods')
      },
      {
        path: '/setting/expressdoc',
        name: '电子面单',
        component: () => import('@/views/setting/expressdoc')
      },
      {
        path: '/setting/viewconfig',
        name: '页面配置',
        component: () => import('@/views/setting/viewconfig')
      },
      {
        path: '/setting/uu',
        name: 'UU跑腿',
        component: () => import('@/views/setting/uu')
      },
      {
        path: '/setting/dianwoda',
        name: '点我达',
        component: () => import('@/views/setting/dianwoda')
      },
      {
        path: '/setting/yly',
        name: '易联云',
        component: () => import('@/views/setting/yly')
      },
      {
        path: '/setting/posters',
        name: '分享海报',
        component: () => import('@/views/setting/posters')
      },
      {
        path: '/setting/help',
        name: '常见问题',
        component: () => import('@/views/setting/help')
      }
    ]
  },

  // 日志
  {
    path: '/operationlog',
    component: Layout,
    name: '日志',
    meta: { title: 'custormers', icon: 'a日志' },
    children: [
      {
        path: '/operationlog',
        name: '操作记录',
        component: () => import('@/views/operationlog')
      }
    ]
  },

  // 店铺装修，直接跳转旧后台
  {
    path: '/goToOld',
    component: Layout,
    name: '装修',
    meta: { title: 'custormers', icon: 'a日志' },
    children: [
      {
        path: '/goToOld',
        name: '装修',
        component: () => import('@/views/goToOld')
      }
    ]
  },

  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => {
    document.body.scrollTop = 0;
  },
  routes: constantRoutes
})

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export function loginRouter() {
  router.push({ path: '/login' })
}

export default router
